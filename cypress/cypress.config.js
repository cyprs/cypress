const { defineConfig } = require('cypress')

module.exports = defineConfig({
    e2e: {
        setupNodeEvents(on, config) {
            // implement node event listeners here
        },
        baseUrl: 'https://www.amazon.com',
        supportFile: 'cypress/support/e2e.js' // Ensure this path is correct
    },
})
