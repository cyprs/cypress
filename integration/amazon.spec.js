describe('Amazon Website Test', () => {
    it('should open Amazon website', () => {
        cy.visit('https://www.amazon.com')
        cy.title().should('include', 'Amazon')
    })
})
