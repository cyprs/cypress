# Use the official Cypress image with the latest stable version
FROM cypress/included:latest

# Set the working directory
WORKDIR /e2e

# Create package.json and package-lock.json files
RUN npm init -y

# Install Cypress
RUN npm install cypress --save-dev

# Install Allure commandline
RUN apt-get update && apt-get install -y openjdk-11-jre curl
RUN curl -o allure.tgz -L https://github.com/allure-framework/allure2/releases/download/2.13.9/allure-2.13.9.tgz
RUN tar -zxvf allure.tgz -C /opt/
RUN ln -s /opt/allure-2.13.9/bin/allure /usr/bin/allure

# Create Cypress configuration file
RUN echo "module.exports = {" > cypress.config.js && \
    echo "  e2e: {" >> cypress.config.js && \
    echo "    baseUrl: 'https://www.amazon.com'," >> cypress.config.js && \
    echo "    supportFile: false," >> cypress.config.js && \
    echo "    setupNodeEvents(on, config) {" >> cypress.config.js && \
    echo "      // implement node event listeners here" >> cypress.config.js && \
    echo "    }" >> cypress.config.js && \
    echo "  }" >> cypress.config.js && \
    echo "}" >> cypress.config.js

# Create Cypress folder structure and add the Amazon test
RUN mkdir -p cypress/e2e && \
    echo "describe('Amazon Test', () => {" > cypress/e2e/amazon_spec.cy.js && \
    echo "  it('Visits Amazon and checks if the site loads', () => {" >> cypress/e2e/amazon_spec.cy.js && \
    echo "    cy.visit('/')" >> cypress/e2e/amazon_spec.cy.js && \
    echo "    cy.get('body').should('be.visible')" >> cypress/e2e/amazon_spec.cy.js && \
    echo "  })" >> cypress/e2e/amazon_spec.cy.js && \
    echo "})" >> cypress/e2e/amazon_spec.cy.js

# Run Cypress tests
ENTRYPOINT ["npx", "cypress", "run"]