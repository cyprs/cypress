const allureWriter = require('@shelex/cypress-allure-plugin/writer');
const { defineConfig } = require('cypress');

module.exports = defineConfig({
    e2e: {
        setupNodeEvents(on, config) {
            allureWriter(on, config);
            return config;
        },
        baseUrl: 'https://www.amazon.com',
        supportFile: 'support/e2e.js',
        video: false, // Disable video recording
        chromeWebSecurity: false,
        env: {
            allureResultsPath: 'allure-results'
        }
    },
    reporter: 'allure',
    reporterOptions: {
        outputDir: 'allure-results',
    },
});
